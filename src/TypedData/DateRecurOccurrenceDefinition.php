<?php

namespace Drupal\date_recur_sapi\TypedData;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * @see \Drupal\date_recur_sapi\Plugin\DataType\DateRecurOccurrence
 */
class DateRecurOccurrenceDefinition extends MapDataDefinition {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    return [
      'start_date' => DataDefinition::create('datetime_iso8601')
        ->setLabel($this->t('Start date'))
        ->setDescription($this->t('The computed start DateTime object.')),
      'end_date' => DataDefinition::create('datetime_iso8601')
        ->setLabel($this->t('Start date'))
        ->setDescription($this->t('The computed start DateTime object.')),
      'field_delta' => DataDefinition::create('integer')
        ->setLabel($this->t('Field delta'))
        ->setDescription($this->t('The field delta used to generate the occurrence.')),
      'entity' => EntityDataDefinition::create()
        ->setLabel($this->t('Entity')),
    ];
  }

}
