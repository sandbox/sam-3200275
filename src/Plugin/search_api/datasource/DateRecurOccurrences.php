<?php

namespace Drupal\date_recur_sapi\Plugin\search_api\datasource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\date_recur\DateRange;
use Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\search_api\Plugin\search_api\datasource\ContentEntity;

/**
 * A datasource for occurrences of a recurring date.
 *
 * This datasource extends the existing ContentEntity datasource in order to
 * make use of several features including the rendering of the underlying entity
 * and the access checking.
 *
 * @SearchApiDatasource(
 *   id = "date_recur_occurrences",
 *   deriver = "Drupal\date_recur_sapi\Plugin\search_api\datasource\DateRecurOccurrencesDeriver"
 * )
 */
class DateRecurOccurrences extends ContentEntity {

  /**
   * How many entities to process at once when rebuilding the tracker.
   */
  const PAGE_SIZE = 5;

  /**
   * How far into the future we should create occurrences should be indexed.
   */
  const PRE_CREATE_DURATION = 'P2Y';

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions() {
    return [
      'start_date' => DataDefinition::create('datetime_iso8601')
        ->setLabel($this->t('Start date'))
        ->setDescription($this->t('The computed start DateTime object.')),
      'end_date' => DataDefinition::create('datetime_iso8601')
        ->setLabel($this->t('End date'))
        ->setDescription($this->t('The computed start DateTime object.')),
      'field_delta' => DataDefinition::create('integer')
        ->setLabel($this->t('Field delta'))
        ->setDescription($this->t('The field delta used to generate the occurrence.')),
      'entity' => EntityDataDefinition::create($this->getEntityTypeId())
        ->setLabel($this->t('Entity')),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getItemId(ComplexDataInterface $item) {
    $value = $item->getValue();
    $date_range = new DateRange(new \DateTime($value['start_date']), new \DateTime($value['end_date']));
    return sprintf('%s:%s:%s', $value['entity']->id(), $value['field_delta'], static::buildOccurrenceDeltaIdentifier($date_range));
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids) {
    $id_parts = array_map(function ($id) {
      $parts = explode(':', $id);
      return [
        'entity_id' => $parts[0],
        'field_delta' => $parts[1],
        'occurrence_id' => $parts[2],
      ];
    }, array_combine($ids, $ids));

    // Load all entities for the set of occurrences being indexed.
    $entities = $this->getEntityStorage()->loadMultiple(array_column($id_parts, 'entity_id'));
    $all_occurrences = [];
    foreach ($entities as $entity) {
      $all_occurrences = array_merge($all_occurrences, $this->generateOccurrences($entity, $this->getFieldName()));
    }

    $loaded_items = [];
    $item_definition = $this->getTypedDataManager()->createDataDefinition('date_recur_occurrence');

    foreach ($ids as $id) {
      $occurrence = $all_occurrences[$id];
      $loaded_items[$id] = $this->getTypedDataManager()->create($item_definition, [
        'start_date' => $this->formatDatetimeForStorage($occurrence->getStart()),
        'end_date' => $this->formatDatetimeForStorage($occurrence->getEnd()),
        'entity' => $entities[$id_parts[$id]['entity_id']],
        'field_delta' => $id_parts[$id]['field_delta'],
      ]);
    }

    return $loaded_items;
  }

  /**
   * Format a datetime object for storage.
   *
   * @param \DateTime $datetime
   *   A datetime object.
   *
   * @return string
   *   The formatted time.
   */
  protected function formatDatetimeForStorage(\DateTime $datetime): string {
    $cloned = clone $datetime;
    $cloned->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    return $cloned->format('c');
  }

  /**
   * {@inheritdoc}
   */
  public function getItemIds($page = NULL) {
    $storage = $this->getEntityStorage();
    // We must start with an entity query to build tracking information, since
    // the date_recur table tracks all revision IDs, we cannot know which
    // records from that table are associated with the default revision of any
    // given entity when querying for them directly.
    $entities_query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->exists($this->getFieldName());

    // The pager will increment as long as NULL isn't returned from this method.
    if ($page !== NULL) {
      $entities_query->range($page * static::PAGE_SIZE, static::PAGE_SIZE);
    }
    $all_entities = $entities_query->execute();
    if (empty($all_entities)) {
      return NULL;
    }

    // Load the occurrences and extract their identifiers.
    $loaded_entities = $storage->loadMultiple($all_entities);
    $all_occurrences = [];
    foreach ($loaded_entities as $entity) {
      $all_occurrences = array_merge($all_occurrences, array_keys($this->generateOccurrences($entity, $this->getFieldName())));
    }

    return $all_occurrences;
  }

  /**
   * Load a list of all occurrences for the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $fieldName
   *   The field name to get occurrences from.
   *
   * @return array
   *   An array of all occurrences keyed by the search API identifier.
   */
  public static function generateOccurrences(EntityInterface $entity, string $fieldName): array {
    $occurrences = [];
    foreach ($entity->{$fieldName} as $field_item_delta => $date_recur_item) {
      $item_occurrences = static::getApplicableItemReccurrences($date_recur_item);
      foreach ($item_occurrences as $item_occurrence) {
        $occurrences[sprintf('%s:%s:%s', $entity->id(), $field_item_delta, static::buildOccurrenceDeltaIdentifier($item_occurrence))] = $item_occurrence;
      }
    }
    return $occurrences;
  }

  /**
   * Build an identifier for a date range occurrence.
   *
   * @param \Drupal\date_recur\DateRange $occurrence
   *   The occurrence.
   *
   * @return string
   *   The identifier.
   */
  protected static function buildOccurrenceDeltaIdentifier(DateRange $occurrence): string {
    return implode('--', array_map(function ($item) {
      $cloned = clone $item;
      $cloned->setTimeZone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      return $cloned->format('Y-m-d-H-i-s');
    }, [$occurrence->getStart(), $occurrence->getEnd()]));
  }

  /**
   * Load the reccurrences for a specific field item.
   *
   * @param \Drupal\date_recur\Plugin\Field\FieldType\DateRecurItem $item
   *   The field item.
   *
   * @return \Drupal\date_recur\DateRange[]
   *   A list of occurrences.
   */
  protected static function getApplicableItemReccurrences(DateRecurItem $item): array {
    $until = (new \DateTime('now'))->add(new \DateInterval(static::PRE_CREATE_DURATION));
    return $item->getHelper()->getOccurrences(NULL, $until);
  }

  /**
   * {@inheritdoc}
   *
   * A key override of the parent class, allows us to substitute the entity
   * attached to the recurrence for the entity being indexed, while performing
   * key operations such as rendering HTML and access checks.
   */
  protected function getEntity(ComplexDataInterface $item) {
    $value = $item->getValue();
    return $value['entity'] instanceof EntityInterface ? $value['entity'] : NULL;
  }

  /**
   * Get the date recur field storage.
   *
   * @return \Drupal\Core\Field\FieldStorageDefinitionInterface
   *   The date recur field storage definition.
   */
  protected function getDateRecurStorageDefinition(): FieldStorageDefinitionInterface {
    return $this->getEntityFieldManager()
      ->getFieldStorageDefinitions($this->getEntityTypeId())[$this->getFieldName()];
  }

  /**
   * Get the date recur field name.
   *
   * @return string
   *   The date recur field name.
   */
  protected function getFieldName(): string {
    return $this->pluginDefinition['field_name'];
  }

}
