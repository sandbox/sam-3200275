<?php

namespace Drupal\date_recur_sapi\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Date recur data type.
 *
 * @DataType(
 *   id = "date_recur_occurrence",
 *   label = @Translation("Date recur occurrence"),
 *   definition_class = "\Drupal\date_recur_sapi\TypedData\DateRecurOccurrenceDefinition"
 * )
 */
class DateRecurOccurrence extends Map {
}
