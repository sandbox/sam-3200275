<?php

namespace Drupal\Tests\date_recur_sapi\Kernel;

use Drupal\Core\Entity\EntityStorageInterface;

/**
 * @coversDefaultClass \Drupal\date_recur_sapi\DateRecurSearchApiTrackerSubscriber
 */
class DateRecurSearchApiTrackerSubscriberTest extends DateRecurSapiKernelTestBase {

  /**
   * A mock tracker.
   *
   * @var \Drupal\search_api\Tracker\TrackerInterface
   */
  protected $mockTracker;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $index = $this->createTestIndex();
    $this->mockTracker = new LoggingTrackerDecorator([], 'default', [], $index->getTrackerInstance());
    $index->setTracker($this->mockTracker);

    $index_storage = $this->prophesize(EntityStorageInterface::class);
    $index_storage->loadMultiple()->willReturn([$index]);

    // Ensure the loadMultiple call to the SAPI index storage will return the
    // version with the mocked tracker.
    $entity_type_manager = $this->container->get('entity_type.manager');
    $manager_reflection = new \ReflectionObject($entity_type_manager);
    $handlers = $manager_reflection->getProperty('handlers');
    $handlers->setAccessible(TRUE);
    $handlers->setValue($entity_type_manager, [
      'storage' => [
        'search_api_index' => $index_storage->reveal(),
      ],
    ]);
  }

  /**
   * @covers ::onSave
   * @covers ::onEntityDelete
   */
  public function testOnSaveFullEntityLifecycle() {
    // Creating the entity will result in a series of initial inserts into the
    // tracker.
    $entity = $this->createTestEntity();
    $this->assertTrackerState([
      'trackItemsInserted',
      [
        'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
      ],
    ]);

    // Increase the count of the rrule for the second field item from 5 to 10,
    // this will result in a series of inserts and updates into the tracker.
    $entity->dr[1]->rrule = 'FREQ=WEEKLY;BYDAY=WE,TH,FR;COUNT=10';
    $entity->save();
    $this->assertTrackerState([
      [
        'trackItemsInserted',
        [
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-27-23-00-00--2011-12-27-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-28-23-00-00--2011-12-28-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-29-23-00-00--2011-12-29-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2012-01-03-23-00-00--2012-01-03-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2012-01-04-23-00-00--2012-01-04-23-30-00',
        ],
      ],
      [
        'trackItemsUpdated',
        [
          'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
        ],
      ],
    ]);

    // Remove the first field item, which will result in a series of deletions
    // and insertions. These are tracked as insertaions and not updates, since
    // the ID of the recurrences for delta 1 are rekeyed to delta 0.
    $entity->dr->removeItem(0);
    $entity->save();
    $this->assertTrackerState([
      [
        'trackItemsDeleted',
        [
          'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-27-23-00-00--2011-12-27-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-28-23-00-00--2011-12-28-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2011-12-29-23-00-00--2011-12-29-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2012-01-03-23-00-00--2012-01-03-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:1:2012-01-04-23-00-00--2012-01-04-23-30-00',
        ],
      ],
      [
        'trackItemsInserted',
        [
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-14-23-00-00--2011-12-14-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-15-23-00-00--2011-12-15-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-20-23-00-00--2011-12-20-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-21-23-00-00--2011-12-21-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-22-23-00-00--2011-12-22-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-27-23-00-00--2011-12-27-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-28-23-00-00--2011-12-28-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-29-23-00-00--2011-12-29-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2012-01-03-23-00-00--2012-01-03-23-30-00',
          'date_recur_occurrences:dr_entity_test__dr/1:0:2012-01-04-23-00-00--2012-01-04-23-30-00',
        ],
      ],
    ]);

    // Delete the entity, which will result in a delete for the remaining items.
    $entity->delete();
    $this->assertTrackerState([
      'trackItemsDeleted',
      [
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-14-23-00-00--2011-12-14-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-15-23-00-00--2011-12-15-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-20-23-00-00--2011-12-20-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-21-23-00-00--2011-12-21-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-22-23-00-00--2011-12-22-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-27-23-00-00--2011-12-27-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-28-23-00-00--2011-12-28-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2011-12-29-23-00-00--2011-12-29-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2012-01-03-23-00-00--2012-01-03-23-30-00',
        'date_recur_occurrences:dr_entity_test__dr/1:0:2012-01-04-23-00-00--2012-01-04-23-30-00',
      ],
    ]);
  }

  /**
   * Assert calls expected to the tracker and reset it's internal state.
   */
  protected function assertTrackerState(array $expectedCalls): void {
    // Unroll the format of expected calls in the case where there is only one
    // call to simplify the assertion format.
    $actual_calls = count($this->mockTracker->calls) === 1 ? $this->mockTracker->calls[0] : $this->mockTracker->calls;
    try {
      $this->assertEquals($expectedCalls, $actual_calls);
    }
    catch (\Exception $e) {
      $this->fail(sprintf("Calls to the tracker did not match the expected calls, expected calls were: \n\n %s", var_export($actual_calls, TRUE)));
    }
    $this->mockTracker->reset();
  }

}
