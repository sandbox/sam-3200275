<?php

namespace Drupal\Tests\date_recur_sapi\Kernel;

use Drupal\date_recur_entity_test\Entity\DrEntityTest;
use Drupal\KernelTests\KernelTestBase;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Entity\Server;
use Drupal\search_api\IndexInterface;

/**
 * A base class for Kernel tests.
 */
class DateRecurSapiKernelTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'search_api',
    'date_recur',
    'date_recur_sapi',
    'date_recur_entity_test',
    'search_api_db',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('search_api_task');
    $this->installSchema('search_api', ['search_api_item']);
    $this->installEntitySchema('dr_entity_test');
    $this->installEntitySchema('user');
  }

  /**
   * Get a test entity with some rules.
   */
  protected function createTestEntity(): DrEntityTest {
    $entity = DrEntityTest::create([
      'dr' => [
        [
          'value' => '2014-06-15T23:00:00',
          'end_value' => '2014-06-16T07:00:00',
          'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=3',
          'infinite' => '0',
          'timezone' => 'Australia/Sydney',
        ],
        [
          'value' => '2011-12-14T23:00:00',
          'end_value' => '2011-12-14T23:30:00',
          'rrule' => 'FREQ=WEEKLY;BYDAY=WE,TH,FR;COUNT=5',
          'infinite' => '0',
          'timezone' => 'Australia/Sydney',
        ],
      ],
    ]);
    $entity->save();
    return $entity;
  }

  /**
   * Create a test index.
   */
  protected function createTestIndex(): IndexInterface {
    $server = Server::create([
      'id' => 'test_server',
      'name' => 'Test server',
      'status' => TRUE,
      'backend' => 'search_api_db',
      'backend_config' => [
        'min_chars' => 3,
        'database' => 'default:default',
      ],
    ]);
    $server->save();
    $index = Index::create([
      'name' => 'Test Index',
      'id' => 'test_index',
      'status' => TRUE,
      'server' => 'test_server',
      'datasource_settings' => [
        'date_recur_occurrences:dr_entity_test__dr' => [],
      ],
      'tracker_settings' => [
        'default' => [],
      ],
    ]);
    $index->save();
    return $index;
  }

}
