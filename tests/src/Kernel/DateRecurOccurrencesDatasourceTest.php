<?php

namespace Drupal\Tests\date_recur_sapi\Kernel;

use Drupal\date_recur_entity_test\Entity\DrEntityTest;

/**
 * @coversDefaultClass \Drupal\date_recur_sapi\Plugin\search_api\datasource\DateRecurOccurrences
 * @covers \Drupal\date_recur_sapi\Plugin\search_api\datasource\DateRecurOccurrencesDeriver
 * @covers \Drupal\date_recur_sapi\Plugin\DataType\DateRecurOccurrence
 * @covers \Drupal\date_recur_sapi\TypedData\DateRecurOccurrenceDefinition
 */
class DateRecurOccurrencesDatasourceTest extends DateRecurSapiKernelTestBase {

  /**
   * The test datasource.
   *
   * @var \Drupal\search_api\Datasource\DatasourceInterface
   */
  protected $datasource;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->datasource = $this->createTestIndex()->getDatasource('date_recur_occurrences:dr_entity_test__dr');
  }

  /**
   * @covers ::getItemIds
   */
  public function testGetItemIdsAll() {
    $this->createTestEntity();
    $occurrence_ids = $this->datasource->getItemIds();
    $this->assertEquals([
      '1:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
      '1:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
      '1:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
      '1:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
      '1:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
      '1:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
      '1:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
      '1:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
    ], $occurrence_ids);
  }

  /**
   * @covers ::getItemIds
   */
  public function testGetItemIdsPaged() {
    array_map(function () {
      return $this->createTestEntity();
    }, range(0, 6));

    $pages = [
      [
        '1:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '1:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '1:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '1:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '1:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '1:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '1:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '1:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
        '2:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '2:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '2:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '2:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '2:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '2:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '2:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '2:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
        '3:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '3:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '3:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '3:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '3:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '3:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '3:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '3:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
        '4:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '4:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '4:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '4:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '4:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '4:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '4:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '4:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
        '5:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '5:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '5:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '5:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '5:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '5:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '5:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '5:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
      ],
      [
        '6:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '6:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '6:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '6:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '6:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '6:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '6:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '6:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
        '7:0:2014-06-15-23-00-00--2014-06-16-07-00-00',
        '7:0:2014-06-16-23-00-00--2014-06-17-07-00-00',
        '7:0:2014-06-17-23-00-00--2014-06-18-07-00-00',
        '7:1:2011-12-14-23-00-00--2011-12-14-23-30-00',
        '7:1:2011-12-15-23-00-00--2011-12-15-23-30-00',
        '7:1:2011-12-20-23-00-00--2011-12-20-23-30-00',
        '7:1:2011-12-21-23-00-00--2011-12-21-23-30-00',
        '7:1:2011-12-22-23-00-00--2011-12-22-23-30-00',
      ],
    ];

    foreach ($pages as $page_id => $expected_ids) {
      $occurrence_ids = $this->datasource->getItemIds($page_id);
      $this->assertEquals($expected_ids, $occurrence_ids);
    }
  }

  /**
   * @covers ::getItemIds
   */
  public function testGetItemIdsNoEntity() {
    $this->assertNull($this->datasource->getItemIds());
  }

  /**
   * @covers ::loadMultiple
   */
  public function testLoadMultiple() {
    $item_definition = \Drupal::typedDataManager()->createDataDefinition('date_recur_occurrence');
    $entity = $this->createTestEntity();

    $expected_items = [
      '1:0:2014-06-15-23-00-00--2014-06-16-07-00-00' => \Drupal::typedDataManager()
        ->create($item_definition, [
          'start_date' => '2014-06-15T23:00:00+00:00',
          'end_date' => '2014-06-16T07:00:00+00:00',
          'entity' => DrEntityTest::load($entity->id()),
          'field_delta' => 0,
        ]),
      '1:1:2011-12-14-23-00-00--2011-12-14-23-30-00' => \Drupal::typedDataManager()
        ->create($item_definition, [
          'start_date' => '2011-12-14T23:00:00+00:00',
          'end_date' => '2011-12-14T23:30:00+00:00',
          'entity' => DrEntityTest::load($entity->id()),
          'field_delta' => 1,
        ]),
    ];

    $items = $this->datasource->loadMultiple(array_keys($expected_items));
    $this->assertEquals($expected_items, $items);
  }

  /**
   * @covers ::getItemId
   */
  public function testGetItemId1() {
    $item_definition = \Drupal::typedDataManager()->createDataDefinition('date_recur_occurrence');
    $entity = $this->createTestEntity();
    $item = \Drupal::typedDataManager()->create($item_definition, [
      'start_date' => '2011-12-14T23:00:00+00:00',
      'end_date' => '2011-12-14T23:30:00+00:00',
      'entity' => $entity,
      'field_delta' => 1,
    ]);
    $this->assertSame('1:1:2011-12-14-23-00-00--2011-12-14-23-30-00', $this->datasource->getItemId($item));
  }

  /**
   * Get a test entity with some rules.
   */
  protected function createTestEntity(): DrEntityTest {
    $entity = DrEntityTest::create([
      'dr' => [
        [
          'value' => '2014-06-15T23:00:00',
          'end_value' => '2014-06-16T07:00:00',
          'rrule' => 'FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR;COUNT=3',
          'infinite' => '0',
          'timezone' => 'Australia/Sydney',
        ],
        [
          'value' => '2011-12-14T23:00:00',
          'end_value' => '2011-12-14T23:30:00',
          'rrule' => 'FREQ=WEEKLY;BYDAY=WE,TH,FR;COUNT=5',
          'infinite' => '0',
          'timezone' => 'Australia/Sydney',
        ],
      ],
    ]);
    $entity->save();
    return $entity;
  }

}
